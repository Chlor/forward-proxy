'use strict'

const http            = require('http'),
      url             = require('url'),
      net             = require('net'),
      ws              = require('ws'),
      websocketStream = require('websocket-stream')

const handler = (req, clientRes) => {
  let parsed   = url.parse(req.url, true),
      proxyReq = http.request(Object.assign(parsed, {
        method: 'GET',
        headers: req.headers
      }), proxyRes => {
        Object.keys(proxyRes.headers)
          .forEach(key => clientRes.setHeader(key, proxyRes.headers[key]))
        proxyRes.pipe(clientRes)
      })

  proxyReq.end()
}

let server = http.createServer(handler),
    wss    = new ws.Server({server}),
    wss2   = new ws.Server({port: 8081})

//wss2 is for debug only - remove it
wss2.on('connection', socket => {
  socket.on('message', msg => {
    socket.send(msg.toString().split('').reverse().join(''))
  })
})

wss.on('connection', socket => {
  let proxyWS = new ws('ws://localhost:8081'), // get host and port?? from incoming socket.upgradeReq.url
      stream  = websocketStream(socket)
  stream.pipe(websocketStream(proxyWS))
  proxyWS.on('message', msg => {
    socket.send(msg)
  })

})

server.on('connect', (req, socket) => {
  let serverUrl = url.parse('https://' + req.url),
      srvSocket = net.connect(serverUrl.port, serverUrl.hostname, () => {
        socket.write('HTTP/1.1 200 Connection Established\r\n' +
          'Proxy-agent: Node-Proxy\r\n' +
          '\r\n')
        srvSocket.pipe(socket)
        socket.pipe(srvSocket)
      })
})

server.on('error', err => console.log(err))

server.listen(8080, () => {
  console.info('listening on 8080')
})
