const hoxy  = require('hoxy'),
      fs    = require('fs'),
      ws    = require('websocket-stream'),
      PORT  = 8080,
      proxy = hoxy.createServer({
        certAuthority: {
          key: fs.readFileSync('./ssl/dummy-ca.key.pem'),
          cert: fs.readFileSync('./ssl/dummy-ca.crt.pem')
        }
      })

// https?
proxy.intercept({
  phase: 'response',
  mimeType: 'text/html',
  as: 'string'
}, (req, res) => {
  res.string = res.string.replace(/Bill Gates/g, 'Józef Piłsudski')
})

proxy.intercept({
  phase: 'response',
  mimeType: 'text/html',
  as: '$'
}, (req, res) => {
  res.$('title').html('DUPA')
})

// websocket
const socketHandler = function (incoming) {
  // port: socket.upgradeReq.headers['x-forwarded-for'] || socket.upgradeReq.connection.remoteAddress
  // host: socket.upgradeReq.url
  let outgoing = new ws('ws://localhost:8081')

  incoming.pipe(outgoing).pipe(incoming)
  outgoing.pipe(process.stdout)

}

const socket2Handler = socket => {
  socket.pipe(socket)
}

const wss  = new ws.createServer({server: proxy._server}, socketHandler),
      wss2 = new ws.createServer({port: 8081}, socket2Handler)


proxy.listen(8080, () => console.log(`Listening on ${PORT}`))

proxy.on('error', err => console.error(err))
